## ~/.dotfiles
![photo01](https://i.imgur.com/5jHtWB9.jpg)

![photo02](https://i.imgur.com/eZVawF7.png)


| OS       	| Linux          	|
|-----------|-----------------|
| Distro 	| EndeavourOS          	|
| Terminal 	| Kitty          	|
| Shell    	| zsh            	|
| WM       	| bspwm          	|
| Font     	| JetBrains Mono 	|
| Editor   	| vim            	|
