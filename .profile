# Editor 
export EDITOR='vim'

# Terminal
export TERMINAL='kitty'

# Browser
export BROWSER='brave'

# Qt
export QT_STYLE_OVERRIDE=kvantum

# XDG Paths
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

# Java
export JAVA_HOME=/usr/lib/jvm/default
export PATH=$JAVA_HOME/bin:$PATH
export _JAVA_AWT_WM_NONREPARENTING=1

# Android
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator

# Composer
export PATH=$PATH:$HOME/.config/composer/vendor/bin

# Dotnet
export DOTNET_ROOT=/usr/share/dotnet
export DOTNET_CLI_TELEMETRY_OPTOUT=1.
export PATH=$DOTNET_ROOT:$PATH

# Local scripts
export PATH=$HOME/.bin:$PATH
